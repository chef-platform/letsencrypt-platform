#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'openssl'

# Open port 80 on the server if it is not already opened and close it once
# the certificates are generated.
is_80_closed = `sudo iptables -L -n| grep '^ACCEPT.*dpt:80$'`.empty?
execute 'open_iptables_on_port_80' do
  command 'iptables -I INPUT 1 -p tcp -m tcp --dport 80 -j ACCEPT'
  action :nothing
  only_if is_80_closed.to_s
end

execute 'close_iptables_on_port_80' do
  command 'iptables -D INPUT -p tcp -m tcp --dport 80 -j ACCEPT'
  action :nothing
  only_if is_80_closed.to_s
end

# Create the certificates if listed in node[cookbook_name]['certificates'].
# Already existing vertificates are not created (see renew below).
node[cookbook_name]['certificates'].each_pair do |fqdn, domains|
  fqdn_only = domains.nil? || domains.empty?
  authenticator = "-a #{node[cookbook_name]['authenticator']}"
  webroot = "-w #{node[cookbook_name]['webroot_path']}"
  domains = "-d #{fqdn} -d #{domains.join(' -d ')}" unless fqdn_only
  domains = "-d #{fqdn}" if fqdn_only
  email = "-m #{node[cookbook_name]['email']}"
  agree_tos = '--agree-tos'
  args = "#{authenticator} #{webroot} #{domains} #{email} #{agree_tos}"
  next if File.exist?("#{node[cookbook_name]['live_path']}/#{fqdn}")
  execute 'Create certs' do
    command "certbot certonly #{args}"
    action :run
    notifies :run, 'execute[open_iptables_on_port_80]', :before
    notifies :run, 'execute[close_iptables_on_port_80]', :delayed
  end
end

# Try to renew certificates only it is valid for less than 30 days.
Dir["#{node[cookbook_name]['live_path']}/*"].each do |certs_dir|
  certfile = File.read("#{certs_dir}/fullchain.pem")
  cert = OpenSSL::X509::Certificate.new(certfile)
  renew_date = cert.not_after.to_i - 2_592_000
  next if Time.now.to_i < renew_date
  execute 'Renew certs' do
    command 'certbot renew'
    action :run
    notifies :run, 'execute[open_iptables_on_port_80]', :before
    notifies :run, 'execute[close_iptables_on_port_80]', :delayed
  end
end
