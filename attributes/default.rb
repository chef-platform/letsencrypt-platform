default['letsencrypt-platform']['pkgs'] = [
  'epel-release',
  'certbot',
  'nginx'
]

default['letsencrypt-platform']['certificates'] = {}

default['letsencrypt-platform']['authenticator'] = 'webroot'
default['letsencrypt-platform']['webroot_path'] = '/usr/share/nginx/html'
default['letsencrypt-platform']['live_path'] = '/etc/letsencrypt/live'

default['letsencrypt-platform']['email'] = 'test@example.com'

default['letsencrypt-platform']['challenge']['server'] = node['fqdn']
