name             'letsencrypt-platform'
maintainer       'Sam4Mobile'
maintainer_email 'dps.team@s4m.io'
license          'Apache 2.0'
description      'Installs/Configures letsencrypt-platform'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url       'https://gitlab.com/s4m-chef-repositories/letsencrypt-platform'
issues_url       'https://gitlab.com/s4m-chef-repositories/letsencrypt-platform/issues'
version          '1.0.0'

supports 'centos', '>= 7.0'
