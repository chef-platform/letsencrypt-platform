# letsencrypt-platform Cookbook

This cookbook installs and configures the let's encrypt environment needed
in order to create or renew certificates.

## Requirements

### Chef

- Chef 12.0 or later

## Attributes

### letsencrypt-platform::default

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['pkgs']</tt></td>
    <td>Array</td>
    <td>Array of needed packages</td>
    <td><tt>['epel-release', 'certbot', 'nginx']</tt></td>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['certificates']</tt></td>
    <td>Hash</td>
    <td>Hash of domains per certificates (keys are fqdn)</td>
    <td><tt>{}</tt></td>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['authenticator']</tt></td>
    <td>String</td>
    <td>Type of the authenticator used</td>
    <td><tt>webroot</tt></td>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['webroot_path']</tt></td>
    <td>String</td>
    <td>Path to the web root directory</td>
    <td><tt>/usr/share/nginx/html</tt></td>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['live_path']</tt></td>
    <td>String</td>
    <td>Path to the current certificates</td>
    <td><tt>/etc/letsencrypt/live</tt></td>
  </tr>
  <tr>
    <td><tt>['letsencrypt-platform']['email']</tt></td>
    <td>String</td>
    <td>Email used to register to let's encrypt CA</td>
    <td><tt>test@example.com</tt></td>
  </tr>
</table>

## Usage

### letsencrypt-platform::default

Just include `letsencrypt-platform` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[letsencrypt-platform]"
  ]
}
```

## Contributing

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

## License and Authors

- Author:: Richard DELAPLACE (<richard.delaplace@s4m.io>)

```text
Copyright (c) 2015-2016 Sam4Mobile

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

[acme]: https://supermarket.chef.io/cookbooks/acme
