# letsencrypt-platform CHANGELOG

This file is used to list changes made in each version of the letsencrypt-platform cookbook.

## 1.0.0
- Richard Delaplace - Initial release of letsencrypt-platform
